import { Component, OnInit, AfterViewInit } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { AnalyticsService } from '../@core/utils/analytics.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../@common/auth/auth.service';
import { ActiveUserModel } from '../@common/model/auth.model';
import { SessionService } from '../@common/service/session.service';
import { Router } from '@angular/router';



@Component({
  selector: 'login',
  styleUrls: ['./login.component.scss'],
  templateUrl: './login-component.html'
})
export class LoginComponent {

  loginForm: FormGroup;



  constructor(
    private _fb: FormBuilder,
    private sessionService: SessionService,
    private authSservice: AuthService,
    private route: Router
  ) {

  }
  // private currentTheme = 'default';

  // /**
  //  * @name constructor
  //  * @param {NbThemeService} themeService 
  //  * @param {AnalyticsService} analyticsService 
  //  */
  // constructor(private themeService: NbThemeService,
  //   private analyticsService: AnalyticsService
  // ) {

  // }
  ngOnInit() {
    this.loginForm = this._fb.group({
      email: [null, Validators.compose([Validators.required, Validators.nullValidator])],
      username: [null, Validators.compose([Validators.required, Validators.nullValidator])]
    })
  }

  ngAfterViewInit() {
    setTimeout(() => {
      document.getElementsByClassName('spinner')[0].setAttribute('style', 'visibility: hidden;');
    }, 100);


  }

  onLogin(data) {
    this.authSservice.getUser(data.value).then(responce => {
      this.route.navigate([this.authSservice.redirectUrl]);
    }).catch(error => {
      alert('Somthing Worng');
    })


    // this.loginService.getUser(data.value).subscribe(
    //   response => {
    //     this.route.navigate([this.authSservice.redirectUrl]);
    //   },
    //   error => {
    //     alert('Something Wrong')
    //   }
    // )


    //   (response) => {
    //   <ActiveUserModel>response;
    //   this.sessionService.setActiveUser(response)
    //   this.rout.navigate['/pages/dashboard'];
    // })


  }


  // toggleTheme() {
  //   this.themeService.changeTheme(this.currentTheme);
  //   this.analyticsService.trackEvent('switchTheme');
  // }
}
