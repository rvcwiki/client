import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginComponent } from './login.component';
import { AuthService } from '../@common/auth/auth.service';
import { AuthGuardService } from '../@common/auth/auth-guard.service';


const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
  path: 'login',
  component: LoginComponent,
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)

  ],
  exports: [
    RouterModule
  ],
  providers: [
    AuthService,
    AuthGuardService,

  ]
})
export class LoginRoutingModule {
}
