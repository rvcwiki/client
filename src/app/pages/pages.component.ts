import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, Routes } from '@angular/router';

import { MENU_ITEMS } from './pages-menu';
import { SessionService } from '../@common/service/session.service';

@Component({
  selector: 'ngx-pages',
  template: `
    <ngx-sample-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-sample-layout>
  `,
})
export class PagesComponent {

  constructor(private userActive: SessionService, protected route: Router) {
  }
 ngOnInit(){
   
  // if (this.userActive.getActiveUser() === null) {
  //   this.route.navigate(['/login']);
  // }

 }

  menu = MENU_ITEMS;
}
