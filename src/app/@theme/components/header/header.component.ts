import { Component, Input, OnInit } from '@angular/core';

import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { UserService } from '../../../@core/data/users.service';
import { AnalyticsService } from '../../../@core/utils/analytics.service';
import { SessionService } from '../../../@common/service/session.service';
import { Router } from '@angular/router';
import { ActiveUserModel } from '../../../@common/model/auth.model';
import { AuthService } from '../../../@common/auth/auth.service';



@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {


  @Input() position = 'normal';

  user: any;
  userMenu = [{ title: 'Profile' }, { title: 'Log out' }];
  userSign: ActiveUserModel = new ActiveUserModel();


  constructor(private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private userService: UserService,
    private analyticsService: AnalyticsService,
    private userActive: SessionService,
    private authService : AuthService,
    private rout: Router) {
  }

  ngOnInit() {
    this.userService.getUsers()
      .subscribe((users: any) => this.user = users.nick);

    if (this.userActive.getActiveUser() !== null) {
      this.userSign = this.userActive.getActiveUser()
    }

  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    return false;
  }

  toggleSettings(): boolean {
    this.sidebarService.toggle(false, 'settings-sidebar');
    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    this.analyticsService.trackEvent('startSearch');
  }
  onLogout() {
    this.userActive.clearActiveUser();
    this.rout.navigate([this.authService.redirectUrlLogout]);
  }

}
