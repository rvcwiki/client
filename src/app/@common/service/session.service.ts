import { Injectable } from '@angular/core';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { ActiveUserModel } from '../model/auth.model';

@Injectable()
export class SessionService {

  constructor(private _sessionStorage: SessionStorageService) { }

  setActiveUser(user: ActiveUserModel) {
    this._sessionStorage.store('ACTIVE_USER', user);
  }

  getActiveUser() {
    return this._sessionStorage.retrieve('ACTIVE_USER');
  }

  clearActiveUser() {
    this._sessionStorage.clear('ACTIVE_USER');
  }

}
