export class ActiveUserModel {
    ID: number;
    first_name: string;
    last_name: string;
    token: string;
}